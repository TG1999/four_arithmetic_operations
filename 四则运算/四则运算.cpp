﻿// 四则运算.cpp : 此文件包含 "main" 函数。程序执行将在此处开始并结束。
//分别能够实现小学一、二、三、四、五年级的四则运算要求， 逐步实现各个年级的难度 
//解决思路：switch进行变量控制，年纪用+，-，*，/，运算个数来分类。一年级只有+，-，二年级三个数进行+，-，三年级*，/，四年级三个数运算，五年级小数运算。()加入其中。
//要求能够通过输入来选择不同年级，每个年级还得区分难，中，易三个等级 
//解决思路，易0-100以内运算，中，100-1000以内运算，难，1000-10000以上运算
//程序支持判断对错及累计得分与时间
//解决思路：对错用count计数，最终算出正确率。时间用clock函数计算
//一次可以出100道题目，而且不能重复（比如2 + 3 与 3 + 2 算重复的）
//采用不重复随机算法，尽量降低重复。
//充分发挥想象增加满足小学生数学检测需要的功能 
//想法：支持循环测验。


#include <iostream>
#include<cstdlib>
#include<time.h>
#define CLOCKS_PER_SEC ((clock_t)1000)
using namespace std;
int Difficulty(int difficulty)
{
    int N=0;
    switch (difficulty)
    {
    case 1:
        N = 100;
        break;
    case 2:
        N = 1000;
        break;
    case 3:
        N = 10000;
        break;
    default:
        cout << "请输入正确的难度。" << endl;
        break;
    }
    return N;
}
void Grade(int grade,int numberExercise,int N) {
    double a1, a2, a3,c,k, correctrate;
    double duration;//做题花费的时间
    clock_t start, finish;
int mode, count = 0;
    switch (grade)
    {
    case 1: //+,-,2
        start = clock();
        srand((unsigned)time(NULL));
        for (int i = 0; i < numberExercise; i++) {
            a1 = rand() % N;
            a2 = rand() % N;
            mode = rand() % 2;
            switch (mode) //确定运算符
            {
            case 0:
                cout << a1<< "+" << a2 << "=" << endl;
                c = a1+a2;
                break;
            case 1:
                c = a1-a2;
                cout << a1 << "-" << a2<< "=" << endl;
                break;
            }
            cout << "请输入答案：";
            cin >> k;
            if (k == c) {
                cout << "恭喜你答对了，请再接再厉" << endl;
                count = count + 1;
            }
            else if (k != c) {
                cout << "虽然答错了，但不要灰心哦，正确答案是" << c << endl;
            }
        }
        correctrate = (count*100) / numberExercise;
        finish = clock();
        duration = (double)(finish - start) / CLOCKS_PER_SEC;
        cout << "恭喜你做完了" << numberExercise << "道题目，做对了" << count << "道题目，正确率为 " << correctrate << "%" <<"  一共耗时(s)："<<duration<<endl;
        break;
    case 2://+-,3
        start = clock();
        srand((unsigned)time(NULL));
        for (int i = 0; i < numberExercise; i++) {
            a1 = rand() % N;
            a2 = rand() % N;
            a3 = rand() % (2*N)-N;
            mode = rand() % 2;
            switch (mode) //确定运算符
            {
            case 0:
                cout << a1 << "+" << a2 <<"+（"<<a3<< "）=" << endl;
                c = a1+a2+a3;
                break;
            case 1:
                c = a1 - a2+a3;
                cout << a1 << "-" << a2 <<"+("<<a3<< ")=" << endl;
                break;
            }
            cout << "请输入答案：";
            cin >> k;
            if (k == c) {
                cout << "恭喜你答对了，请再接再厉" << endl;
                count = count + 1;
            }
            else if (k != c) {
                cout << "虽然答错了，但不要灰心哦，正确答案是" << c << endl;
            }
        }
        correctrate = (count * 100) / numberExercise;
        finish = clock();
        duration = (double)(finish - start) / CLOCKS_PER_SEC;
        cout << "恭喜你做完了" << numberExercise << "道题目，做对了" << count << "道题目，正确率为 " << correctrate << "%" << "  一共耗时(s)：" << duration << endl;
        break;
    case 3://*,/,2
        start = clock();
        srand((unsigned)time(NULL));
        for (int i = 0; i < numberExercise; i++) {
            a1 = rand() % N;
            a2 = rand() % N;
            mode = rand() % 4;
            switch (mode) //确定运算符
            {
            case 0:
                cout << a1 << "+" << a2 << "=" << endl;
                c = a1 + a2;
                break;
            case 1:
                c = a1 - a2;
                cout << a1 << "-" << a2 << "=" << endl;
                break;
            case 2:
                c = a1 * a2;
                cout << a1 << "*" << a2 << "=" << endl;
                break;
            case 3:
                c = a1 / a2;
                cout << a1 << "/" << a2 << "=" << endl;
                break;
            }
            cout << "请输入答案：";
            cin >> k;
            if (k == c) {
                cout << "恭喜你答对了，请再接再厉" << endl;
                count = count + 1;
            }
            else if (k != c) {
                cout << "虽然答错了，但不要灰心哦，正确答案是" << c << endl;
            }
        }
        correctrate = (count * 100) / numberExercise;
        finish = clock();
        duration = (double)(finish - start) / CLOCKS_PER_SEC;
        cout << "恭喜你做完了" << numberExercise << "道题目，做对了" << count << "道题目，正确率为 " << correctrate << "%" << "  一共耗时(s)：" << duration << endl;
        break;
    case 4://+,-,*,/,3
        start = clock();
        srand((unsigned)time(NULL));
        for (int i = 0; i < numberExercise; i++) {
            a1 = rand() % N;
            a2 = rand() % N;
            a3 = rand() % (2 * N) - N;
            mode = rand() % 4;
            switch (mode) //确定运算符
            {
            case 0:
                cout << a1 << "+（" << a2 <<"*("<<a3<<"))=" << endl;
                c = a1 + a2*a3;
                break;
            case 1:
                c = a1 - a2/a3;
                cout << a1 << "-(" << a2 << "/(" << a3 << "))=" << endl;
                break;
            case 2:
                c = a1 * (a2-a3);
                cout << a1 << "*(" << a2 << "-(" << a3 << "))=" << endl;
                break;
            case 3:
                c = a1 / (a2+a3);
                cout << a1 << "/(" << a2 << "+(" << a3 << "))=" << endl;
                break;
            }
            cout << "请输入答案：";
            cin >> k;
            if (k == c) {
                cout << "恭喜你答对了，请再接再厉" << endl;
                count = count + 1;
            }
            else if (k != c) {
                cout << "虽然答错了，但不要灰心哦，正确答案是" << c << endl;
            }
        }
        correctrate = (count * 100) / numberExercise;
        finish = clock();
        duration = (double)(finish - start) / CLOCKS_PER_SEC;
        cout << "恭喜你做完了" << numberExercise << "道题目，做对了" << count << "道题目，正确率为 " << correctrate << "%" << "  一共耗时(s)：" << duration << endl;
        break;
    case 5://小数运算
        start = clock();
        srand((unsigned)time(NULL));
        for (int i = 0; i < numberExercise; i++) {
            a1 = rand() % N+ rand() / double(RAND_MAX);//加上0-1的小数，即可。
            a2 = rand() % N+ rand() / double(RAND_MAX);
            a3 = rand() % (2 * N) - N+ rand() / double(RAND_MAX);
            mode = rand() % 4;
            switch (mode) //确定运算符
            {
            case 0:
                cout << a1 << "+（" << a2 << "*(" << a3 << "))=" << endl;
                c = a1 + a2 * a3;
                break;
            case 1:
                c = a1 - a2 / a3;
                cout << a1 << "-(" << a2 << "/(" << a3 << "))=" << endl;
                break;
            case 2:
                c = a1 * (a2 - a3);
                cout << a1 << "*(" << a2 << "-(" << a3 << "))=" << endl;
                break;
            case 3:
                c = a1 / (a2 + a3);
                cout << a1 << "/(" << a2 << "+(" << a3 << "))=" << endl;
                break;
            }
            cout << "请输入答案：";
            cin >> k;
            if (k == c) {
                cout << "恭喜你答对了，请再接再厉" << endl;
                count = count + 1;
            }
            else if (k != c) {
                cout << "虽然答错了，但不要灰心哦，正确答案是" << c << endl;
            }
        }
        correctrate = (count * 100) / numberExercise;
        finish = clock();
        duration = (double)(finish - start) / CLOCKS_PER_SEC;
        cout << "恭喜你做完了" << numberExercise << "道题目，做对了" << count << "道题目，正确率为 " << correctrate << "%" << "  一共耗时(s)：" << duration << endl;
        break;
    default:
        cout << "请输入正确的年级。" << endl;
        break;
    }
}
int main()
{
    int T;
    do {
        int grade, numberExercise, difficulty, N = 0;
        cout << "请选择您的当前年级：1：一年级，2：二年级，3：三年级，4：四年级，5：五年级。" << endl;
        cin >> grade;
        cout << "请选择难度：1：简单，2：普通，3：困难。" << endl;
        cin >> difficulty;
        cout << "请输入你要做的题数：" << endl;
        cin >> numberExercise;
        N = Difficulty(difficulty);
        Grade(grade, numberExercise, N);
        cout << "如果您想继续做题，请输入1，退出系统请输入其他任意数" << endl;
        cin >> T;
    } while (T==1);
    system("pause");
    return 0;
}


