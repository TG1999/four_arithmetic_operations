﻿#include <cstdio>
#include<cstdlib>
#include<iostream>

#define MIN -99999
using namespace std;
typedef struct positionASum {//创建该结构体保存数组中的最低位，最高位，以及最大和 
	int low;
	int high;
	int sum;
}PositionASum;

PositionASum Max_Crossing_Arr(int a[], int low, int high, int mid) {//求跨越中点的最大子数组 
	int left_sum = MIN;//左半边的最大和 
	int sum = 0;//下标i所到之处的和 
	int i;
	int max_left = 0;//左半边的数组的某一个值的最大值 
	for (i = mid; i >= low; i--) {
		sum += a[i];
		if (sum > left_sum) {//若和值大于最大和则更新之 
			left_sum = sum;
			max_left = i;//记录最大值下标 
		}
	}
	//右半边类似 
	int right_sum = MIN;
	sum = 0;
	int j;
	int max_right = 0;
	for (j = mid + 1; j <= high; j++) {
		sum += a[j];
		if (sum > right_sum) {
			right_sum = sum;
			max_right = j;
		}
	}
	PositionASum ps;//返回跨越中点的最大子数组的最低位，最高位以及最大和 
	ps.low = max_left;
	ps.high = max_right;
	ps.sum = left_sum + right_sum;

	return ps;
}

PositionASum Max_Arr(int a[], int low, int high) { //求一个子数组的最大子数组（子数组和最大） 

	PositionASum left;
	PositionASum right;
	PositionASum mid;
	int midd;
	if (high == low) {//当数组仅有一个元素 
		PositionASum ps;
		ps.low = low;
		ps.high = high;
		ps.sum = a[low];
		return ps;
	}
	else {//递归，分治合并 
		midd = (low + high) / 2;
		//此处执行一次left
		left = Max_Arr(a, low, midd);//递归调用自身找到左半边的最大子数组 

		//此处执行一次right
		right = Max_Arr(a, midd + 1, high);//递归调用找到右半边最大子数组 

		//此处执行一次mid
		mid = Max_Crossing_Arr(a, low, high, midd);//找到跨越中点的最大子数组 

	}
	if (left.sum >= right.sum && left.sum >= mid.sum) {//左半边子数组最大则返回左半边子数组 
		return left;
	}
	else if (right.sum >= left.sum && right.sum >= mid.sum) {//返回右半边子数组 
		return right;
	}
	else {//返回跨越中点子数组 
		return mid;
	}
}

int main()
{
	int A[10] = { -1,4,0,1,-1,3,-2,8,-3,-5 };
	PositionASum result = Max_Arr(A, 0, 9);
	cout << "最大子数组的最低位为A[" << result.low << "]="<< A[result.low]<<" 最高位为A["<< result.high<<"]="<< A[result.high] << "  最大数组之和为：" << result.sum << endl;//输出结果
	return 0;
}